from django.db import models
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db.models.signals import post_save
from django.dispatch import receiver

import os
import sys
import marshal, types
import base64

PLUGINS_PATH = os.path.join(os.path.split(os.path.split(os.path.abspath(__file__))[0])[0], 'plugins')

sys.path.append(PLUGINS_PATH)

debug = False

DATATYPES = ((0, 'String'),
             (1, 'Integer'),
             (2, 'Float'),
             (3, 'True/False'))

settingsDataTypes = {'string': 0,
                     'str': 0,
                     'integer': 1,
                     'int': 1,
                     'float': 2,
                     'bool': 3,
                     'boolean': 3,
                     'true/false': 3}

class Group(models.Model):
    name = models.CharField(max_length=25, verbose_name = '')
    present = models.BooleanField(default = False)

    def __unicode__(self):
        return self.name

class SettingsValue(models.Model):
    description = models.CharField(max_length=40)
    value = models.CharField(max_length=255, blank = True)
    dataType = models.IntegerField(default = 0, choices = DATATYPES)
    group = models.ForeignKey(Group)
    validationFunction = models.TextField(default = '')

    def __unicode__(self):
        return self.description + ':' + self.value

    def clean(self):
        if self.dataType == 1:
            try:
                x = int(self.value)
                self.value = str(x)
            except:
                raise ValidationError('Value should be an integer')

        elif self.dataType == 2:
            try:
                x = float(self.value)
                self.value = str(x)
            except:
                raise ValidationError('Value should be an float')

        elif self.dataType == 3:
            if self.value.lower() in ['true', 'yes', 't', 'y', '1']:
                self.value = 'True'
            elif self.value.lower() in ['false', 'no', 'f', 'n', '0']:
                self.value = 'False'
            else:
                raise ValidationError('Value should be something of True, False, Yes, No')
        if self.validationFunction != '':
            code = marshal.loads(base64.decodestring(self.validationFunction))
            func = types.FunctionType(code, globals(), "validate_func")
            try:
                res = func(self.description, self.value)
            except:
                raise ValidationError('Custom validation failed')

            if res != '':
                raise ValidationError('Illegal value. ' + res)
                



def getSettings(groupName):
    #print 'getPluginSettings called for', moduleName
    returnValue = {}
    try:
        settingsObjects = SettingsValue.objects.filter(group = Group.objects.get(name = groupName))
        for s in settingsObjects:
            if s.dataType == 1:
                returnValue[s.description] = int(s.value)
            elif s.dataType == 2:
                returnValue[s.description] = float(s.value)
            elif s.dataType == 3:
                if s.value.lower() == 'false':
                    returnValue[s.description] = False
                else:
                    returnValue[s.description] = True
            else:
                returnValue[s.description] = s.value

    except:
        print 'SettingGroup does not exist, return something empty'

    return returnValue

def updateSettings(groupName, settingsDict):
    try:
        g = Group.objects.get(name = groupName)
        g.present = True
    except:
        print 'SettingGroup does not exist, create it'
        g = Group(name = groupName)
        g.present = True
    g.save()

    try:
        settingsObjects = SettingsValue.objects.filter(group = g)
        for s in settingsObjects:
            if s.description not in settingsDict:
                print 'Deleteing setting that is not used any longer'
                s.delete()
    except:
        print 'Error deleting old settings'

    for key in settingsDict.keys():
        if not (len(settingsDict[key]) == 2 or len(settingsDict[key]) == 3):
            print 'Bad format of settings dictionary value, should be a touple of datatype and default value and optionaly a validation function'
            return False


        if settingsDict[key][0].lower() not in settingsDataTypes:
            print 'Bad datatype in settings dictionary, should be any of string, integer, float, boolean'
            return False

        dt = settingsDataTypes[settingsDict[key][0].lower()]

        if dt == 1: #Int
            try:
                defaultValue = int(settingsDict[key][1])
            except:
                return False
        elif dt == 2: #Float
            try:
                defaultValue = float(settingsDict[key][1])
            except:
                return False
        elif dt == 3: #Boolean
            if settingsDict[key][1] == True:
                defaultValue = 'True'
            elif settingsDict[key][1] == False:
                defaultValue = 'False'
            else:
                return
        else: #String
            defaultValue = settingsDict[key][1]


        try:
            s = SettingsValue.objects.get(description = key, group = g)
        except:
            print 'SettingsValue does not exist, create it'
            s = SettingsValue(description = key,
                              value = str(defaultValue),
                              dataType = settingsDataTypes[settingsDict[key][0]],
                              group = g)
            s.save()

        if len(settingsDict[key]) == 3:
            s.validationFunction = base64.encodestring(marshal.dumps(settingsDict[key][2].func_code))
            print 'Stored custom validation function'
            s.save()

        else:
            if s.validationFunction != '':
                s.validationFunction = ''
                print 'Cleared custom validation function'
                s.save()
            

def updateSetting(groupName, setting, value):
    try:
        g = Group.objects.get(name = groupName)
    except:
        print 'SettingGroup does not exist when calling updateSetting'
        return False

    try:
        s = SettingsValue.objects.get(group = g, description = setting)

        s.value = str(value)
        s.clean() #To validate value
        s.save()

    except:
        print 'Failed updating setting'
        raise
        return False

    return True




def deleteObsoleteGroups():
    pass
    for g in Group.objects.filter(present = False):
        for s in SettingsValue.objects.filter(group = g):
            print 'Deleting SettingsValue object', s.description
            s.delete()

        print 'Deleting not present group', g.name
        g.delete()

    #Now reset the present setting on each group (so that it is ready for next run)
    for g in Group.objects.all():
        g.present = False
        g.save()


@receiver(post_save, sender=SettingsValue)
#@receiver(post_save, dispatch_uid=__name__, sender=SettingsValue)
def settingsChanged(sender, **kwargs):
    print 'SettingsChanged'
    if debug:
        print kwargs['instance'], 'updated'

    try:
        postToQueue(kwargs['instance'].group.name + ',configuration,changed', 'system')
    except:
        print 'Failed with settingsChanged'
        pass #This will fail on startup as plugins call this on import

from signals.models import postToQueue
#import plugins
