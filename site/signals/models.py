from django.db import models
from django.conf import settings

import datetime
from pytz import timezone
import time

import threading
import Queue

from scanf import scanf_compile

import re

import sys
import os
import traceback

debug = False

PLUGINS_PATH = os.path.join(os.path.split(os.path.split(os.path.abspath(__file__))[0])[0], 'plugins')

sys.path.append(PLUGINS_PATH)

maximumSignalRecursionLevel = 20 #Max number of repost from a signal (to avoid infinite recursion)

signalQueue = Queue.Queue()

DAEMON_INPUT_FIFO = '/home/pi/source/automagically/daemon/daemon_input'
DAEMON_OUTPUT_FIFO = '/home/pi/source/automagically/daemon/daemon_output'


#Post a signal to the queue of signals to process
#returnTrace not functional
def postToQueue(signal, input, returnTrace = False):    
    if isinstance(input, SignalObject):
        repost = True
        level = input.level+1
        origin = input.origin
    else:
        repost = False
        level = 0
        origin = input.strip()
#        t0 = time.time()
    if level >= maximumSignalRecursionLevel:
        print 'Error maximumSignalRecursionLevel reached, not allowed to post more to queue'
        return []
        

    s = SignalObject(signal.strip(), origin, level)
#    print 'New Signal in queue:', s, repost

    if os.environ.get('AUTOMAGICALLY_SIGNALHANDLING', 'N') == 'Y':
        signalQueue.put(s)

    else:
        try:
            daemonInputFifo = open(DAEMON_INPUT_FIFO, 'w')
            daemonInputFifo.write(s.serialize() + '\n')
            daemonInputFifo.close()
            
        except:
            print 'Failed writing to daemonInputFifo'
            raise

    return []

handlerCache = []

def fillHandlerCache():
    global handlerCache
    handlerCache = []
    for h in Handler.objects.all():
        handlerCache.append((h, re.compile(h.regexp)))


class SignalProcessingThread(threading.Thread): 
    def __init__(self):
        if len(handlerCache) == 0:
            if debug:
                print 'Empty handler cache, fill it'
            fillHandlerCache()

        self.doExit = False

        threading.Thread.__init__(self)

    def run(self):
        print 'SignalProcessingThread started'
        while not self.doExit:
#            print 'Doing a get from the queue'
            s = signalQueue.get()

            if s == None:
                print 'Got a none from signalQueue in SignalProcessingThread - exit'
                break

#            print 'Got signal from Queue', s
            
            plugins.processSignal(s)

            for h, regexp in handlerCache:
                found = regexp.match(s.content)
                if found:
                    if hasattr(h, 'transform'):
                        h.transform.doProcess(s, found)
                    elif hasattr(h, 'devicecommand'):
                        h.devicecommand.doProcess(s, found)
                    elif hasattr(h, 'storeglobalvariable'):
                        h.storeglobalvariable.doProcess(s, found)
                    elif hasattr(h, 'findrepeat'):
                        h.findrepeat.doProcess(s, found)   

            signalQueue.task_done()
        print 'Signalprocessing thread stopped'

    def stopThread(self):
        self.doExit = True
        signalQueue.put(None)
        

class SignalObject():
    def __init__(self, content = '', origin = '', level = 0, fromStr = '', timestamp = None):
        if fromStr == '':
            self.content = content
            self.origin = origin
            self.level = level
            if timestamp:
                self.timestamp = timestamp
            else:
                self.timestamp = time.time()
        else:
            p = fromStr.split('#', 3)
            self.content = p[3]
            self.origin = p[0]
            self.level = int(p[1])
            self.timestamp = float(p[2])

    def __str__(self):
        return '  '*self.level + self.origin + '#' + self.content

    def serialize(self):
        return self.origin + '#' + str(self.level) + '#' + str(self.timestamp) + '#' + self.content

re_cache = {}
re_cache_size = 500


class Handler(models.Model):
    desc = models.CharField(max_length=255, verbose_name = 'Description')
    pattern = models.CharField(max_length=255, verbose_name = 'Pattern to match', help_text = "Pattern to match use scanf if you dont know what you doing (That is dont tick box below). To match a string just write the string if you want to match an integer add a %d instead of the number. If match a floating point number (it is requested to have a decimal delimiter) add a %f. To match a generic string add a %s.")
    patternIsRegularExpression = models.BooleanField(default = False, verbose_name = 'Pattern is a regular expression (not a scanf string)')
    regexp = models.CharField(max_length=255, blank=True)

    def __unicode__(self):
        return self.desc

    
    def process(self, signal, trace):
        if self.regexp not in re_cache:
            format_re = re.compile(self.regexp)

            if len(re_cache) > re_cache_size:
                re_cache.clear()

            re_cache[self.regexp] = format_re

        found = re_cache[self.regexp].match(signal.content)

        if found:
            if trace:
                signalTrace.append('  '*(signal.level+1) + 'Processed by Handler ' + self.desc)
            if hasattr(self, 'transform'):
                return self.transform.doProcess(signal, found)
            elif hasattr(self, 'devicecommand'):
                return self.devicecommand.doProcess(signal, found)
            elif hasattr(self, 'storeglobalvariable'):
                return self.storeglobalvariable.doProcess(signal, found)
            elif hasattr(self, 'findrepeat'):
                return self.findrepeat.doProcess(signal, found)
            
        return -1

    def clean(self):
        if self.patternIsRegularExpression:
            self.regexp = self.pattern
        else:
            self.regexp, casts = scanf_compile(self.pattern)
        
        
    def save(self):
        super(Handler, self).save()
        postToQueue('system,signalhandler,changed', 'system')


class Transform(Handler):
    output = models.TextField(max_length=2048, verbose_name = 'Send the following signal(s) on match', help_text = "One signal per line, you may use $1 to include the first parsed variable, $2 for the second etc. If you want to reference global variables include there name with $ before and after and it will be replaced with the variables current value. If a line starts with $EVAL: the remaining of the line will be evaluated as a python expression and the result will be the new signal sent to the queue.")

    def doProcess(self, signal, match):
        for s in self.output.splitlines():
            if s.strip() != '':
                i = 0
                for v in match.groups():
                    i += 1
                    s = s.replace('$' + str(i), v)

                variables = {}
                local = timezone(settings.TIME_ZONE)
                variables['now'] = local.localize(datetime.datetime.now())
                

                for gv in GlobalVariable.objects.all():                    
                    variables[gv.name] = gv.getValue()
                    s = s.replace('$' + gv.name + '$', str(variables[gv.name]))

                if s.startswith('$EVAL:'):
                    try:
                        notAllowed = ['__import__', 'open', 'raw_input', 'eval',
                                      'compile', 'dir', 'execfile', 'file', 'input', 'globals',
                                      'help', 'locals', 'memoryview', 'print', 'reload', 'super']

                        toeval = s[6:]
                        for na in notAllowed:
                            if na in toeval:
                                toeval = None
                                break

                        if debug:
                            print 'Evaluate transform', toeval

                        if(toeval):
                            s = str(eval(toeval, variables, {}))
                        else:
                            s = 'NOT ALLOWED strings in expression to eval'

                    except Exception, err:
                        s = 'FAILED ' + s[1:] + ' (' + str(err) + ')'

                if len(s) > 0:
                    postToQueue(s, signal)




class DeviceCommand(Handler):
    dev = models.ForeignKey('core.Device', verbose_name = 'Device', help_text = "The device that should be sent a command to.")
    cmd = models.ForeignKey('core.Command', verbose_name = 'Command', help_text = "The command to send to the device.")

    def doProcess(self, signal, match):
        self.dev.execute(self.cmd)

        
class StoreGlobalVariable(Handler):
    ParsedVariable = models.PositiveIntegerField(default = 1, verbose_name = 'Parsed variable to store', help_text="Use a number starting from 1")
    Variable = models.ForeignKey('core.GlobalVariable', verbose_name = 'Variable to update')    
    Constant = models.CharField(max_length=80, default = '', blank=True, verbose_name = 'Store this constant instead of parsed variable', help_text = "If this is not blank it will ignore the setting in Parsed variable and instead just store the content of this field.")
    
    def doProcess(self, signal, match):
        try:
            if self.Constant == '':
                self.Variable.updateValue(match.groups()[self.ParsedVariable-1])
            else:
                self.Variable.updateValue(self.Constant)
            
        except:
            print 'Error storing in global variable'



findRepeatTemp = {}

class FindRepeat(Handler):
    nrRepeats = models.PositiveIntegerField(default = 2, verbose_name = '# of repeats to trigger')
    maxWaitTimeMs = models.PositiveIntegerField(default = 1000, verbose_name = 'Max time to get all the repeats (ms)')
    holdOffTimeMs = models.PositiveIntegerField(default = 10000, verbose_name = 'Time to wait after triggered before finding new repeats (ms)')
        
    output = models.TextField(max_length=512, verbose_name = 'Signal(s) to send on trigger')

    def doProcess(self, signal, match):
#        print 'FindRepeat'
        n = datetime.datetime.now()

        tempData = findRepeatTemp.get(self.id, None)

        if self.id not in findRepeatTemp:
            tempData = (n, n, 1)
            #print 'Started new/first FindRepeat'

        elif tempData[2] == 0 and tempData[0] + datetime.timedelta(milliseconds = self.holdOffTimeMs) > datetime.datetime.now():
            #print 'Holdoff'
            pass

        elif tempData[0] + datetime.timedelta(milliseconds = self.maxWaitTimeMs) < datetime.datetime.now():
            #To much time have elapsed
            tempData = (n, n, 1)
            
            #print 'Started new FindRepeat'

        elif tempData[2] + 1 >= self.nrRepeats:

            for s in self.output.splitlines():
                if s.strip() != '':
                    i = 0
                    for v in match.groups():
                        i += 1
                        s = s.replace('$' + str(i), v)

                    postToQueue(s, signal)

            tempData = (n, n, 0)
            #print 'Repeat detected reset object\n\n\n'

        else:
            tempData = (tempData[0], n, tempData[2] + 1)
            #print 'Found another repeat', tempData[2], self.nrRepeats


        findRepeatTemp[self.id] = tempData


#This should be done last
import plugins
try:
    from core.models import GlobalVariable
except:
    pass

