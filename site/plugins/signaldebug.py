from django.db import models
import signals.models
from settings.models import getSettings
import time
import os
import socket
import select
from curses.ascii import isprint
import datetime
import Queue

PLUGIN_NAME = os.path.splitext(os.path.split(__file__)[1])[0] #This will be the filename without .py

debug = False

global currentSettings
currentSettings = {}
workQueue = Queue.Queue()

def signalHandler(signal):
    global currentSettings

    if not currentSettings or signal.content.strip() == PLUGIN_NAME + ',configuration,changed':

        currentSettings = getSettings(PLUGIN_NAME)
        if debug:
            print 'Signaldebug got configuration updated message'
            print currentSettings
        try:
            filepath = os.path.join(os.path.split(os.path.abspath(__file__))[0], 'signaldebug.txt')
            fd = open(filepath, 'w')
            fd.close()
            os.chmod(filepath, 0o666)
        except:
            print 'Error trying to reset content of signaldebug.txt'

    if signal.content == 'terminate':
        workQueue.put(None)



    workQueue.put(signal)

def threadFunc():

    global currentSettings
    currentSettings = getSettings(PLUGIN_NAME)

    while 1:
        s = workQueue.get()
        try:


            if s == None:
                print 'Got a none from signal debug workQueue'
                workQueue.task_done()
                break

            if currentSettings.get('Debug to file', False):

                try:
                    fd = open(os.path.join(os.path.split(os.path.abspath(__file__))[0], 'signaldebug.txt'), 'a')
                    fd.write('%s#%s\n' %(datetime.datetime.now().isoformat(' '), str(s)))
                    fd.close()
                except:
                    if debug:
                        print 'error in signaldebug when writing to file'

            if currentSettings.get('Send errors as email', False):
                if s.content.startswith('error:'):
                    print 'Sending signal as email by posting with new prefix'
                    signals.models.postToQueue('email::Automagically error report:' + s.content[6:], PLUGIN_NAME)

        except:
            if debug:
                print 'error in signaldebug'
                raise

        workQueue.task_done()                


def init():
    settings = {'Debug to file':   ('bool', False),
                'Send errors as email': ('bool', False)}

    return ('', settings, signalHandler, threadFunc)

