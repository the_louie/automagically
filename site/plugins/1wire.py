######
#
# Author: Sonny Vallgren
# Contribution to Automagically 2013-02-03
#
######
from django.db import models
import signals.models
from settings.models import getSettings
import Queue
try:
    import ow
    OW_AVAILABLE = True    
except:
    OW_AVAILABLE = False
       
PLUGIN_NAME = '1wire'
UPDATE_INTERVALL = 60 #Every minute

workQueue = Queue.Queue()
global currentSettings
currentSettings = {}

debug = False 

def signalHandler(signal):

    if debug:
        print '1wire.py got signal', signal.content
    if signal.content == 'terminate':
        workQueue.put(None)
    elif signal.content.strip() == PLUGIN_NAME + ',configuration,changed':
        workQueue.put('update')

def threadFunc():
    
    nextUpdate = 10
    keepRunning = True
    connected = False
    enabled = True
    try:
        currentSettings = getSettings(PLUGIN_NAME)
        enabled = currentSettings['Enabled']
        conStr = currentSettings['IP'] + ':' + str(currentSettings['Port'])
        if debug:
            print conStr
        if enabled:
            ow.init(str(conStr))
            connected = True
    except:
        print '1wire Unable to init owserver'
        connected = False
    while(keepRunning):
        try:
            try:
                s = workQueue.get(True, nextUpdate)
            except:
                s = 'TIMEOUT'
                if debug:
                    print '1wire time to fetch data'
            if s == 'update':
                print '1wire Time to update settings'
                currentSettings = getSettings(PLUGIN_NAME)
                enabled = currentSettings['Enabled']
                if enabled:
                    if debug:
                        print 'Enabled'
                        print '1wire Reinit owserver'
                        print '1wire server IP-address:' + currentSettings['IP']
                        print '1wire Port number:' + str(currentSettings['Port'])
                    try:
                        ow.finish()
                    except:
                        pass
                    try:
                        ow.init(str(currentSettings['IP'] + ':' + str(currentSettings['Port'])))
                        connected = True
                    except:
                        print '1wire Error reinit'
                        connected = False
                    nextUpdate = 10
                else:
                    try:
                        ow.finish()
                    except:
                        pass
                    if debug:
                        print '1wire Not enabled'
                    nextUpdate = 60*60
                continue
            if s == None:
                print '1wire Got a none from workQueue'
                workQueue.task_done()
                keepRunning = False
                try:
                    ow.finish()
                except:
                    print '1wire failed terminate ow-server connection'
                continue
            else:
                if not enabled:
                    nextUpdate = 60*60
                    continue

                nextUpdate = UPDATE_INTERVALL
                if connected and enabled:
                    if debug:
                        print '1wire Reading sensors'
                    try:
                        for sensor in ow.Sensor('/').sensorList():
                            if 'temperature' in sensor.entryList():
                                id = 'ow,sensorID:' + str(sensor)[1:].lstrip(' ')[:15] + ',Type:' + str(sensor)[19:]
                                val = str(round(float(sensor.temperature),1))
                                signals.models.postToQueue( id + ',Value:' + val,PLUGIN_NAME)
                                if debug:
                                    print "Sensor ID: " + str(sensor)[1:] + " Temperature: " + str(round(float(sensor.temperature),1))
                    except:
                        print '1wire Error reading owserver'
                        connected = False
        except:
            print '1wire Error in thread function'
            if debug:
                raise    

def init():
    if OW_AVAILABLE:
        settings = {'Enabled':   ('boolean', False),
                    'IP': ('string', 'localhost'),
                    'Port': ('integer', 4003)}
        return (PLUGIN_NAME, settings, signalHandler, threadFunc)                    
    else:
        return (PLUGIN_NAME, {}, None, None)
