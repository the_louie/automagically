from django.db import models
import signals.models
from settings.models import getSettings
from core.models import DataFetcher
import time
import xml.dom.minidom
import Queue
import traceback
import sys
import urllib2

PLUGIN_NAME = 'datafetcher'

debug = False
            
workQueue = Queue.Queue()


class FetchJob:
    def __init__(self, id, name, interval):
        self.id = id
        self.name = name
        self.interval = interval
        self.donext = 0

    def timeleft(self, now):
        if self.donext <= now:
            return 0
        return self.donext - now

    def fetch(self):
        if debug:
            print 'Fetch -', self.name
        self.donext = time.time() + self.interval

        try:
            DataFetcher.objects.get(pk = self.id).fetch()
        except:
            if debug:
                raise
            
def signalHandler(signal):
    if debug:
        print 'datafetcher plugin got signal', signal.content
    if signal.content == 'terminate':
        workQueue.put(None)
    elif signal.content.strip() == PLUGIN_NAME + ',configuration,changed':
        workQueue.put('update')

def getFetchers():
    fetchers = []

    for df in DataFetcher.objects.filter(enable = True):
        fetchers.append(FetchJob(df.id, df.name, df.fetchInterval*60))

    return fetchers


def threadFunc():
    currentSettings = getSettings('datafetcher')

    keepRunning = True

    fetchers = getFetchers()

    nextUpdate = 5
    while(keepRunning):
        try:
            try:
                s = workQueue.get(True, nextUpdate)
            except:
                s = 'TIMEOUT'
                if debug:
                    print 'Datafetcher time to fetch data (or check if there is anything to do)'


            if s == None:
                print 'Got a none from workQueue'
                workQueue.task_done()
                keepRunning = False
                continue
            elif s == 'update':
                print 'Time to update settings in datafetcher'
                currentSettings = getSettings('datafetcher')
                fetchers = getFetchers()
                nextUpdate = 5
    
            else:
                now = time.time()
                todo = []
                nextUpdate = 60
                for f in fetchers:
                    timeleft = f.timeleft(now)
                    if timeleft == 0:
                        todo.append(f)
                    else:
                        nextUpdate = min(nextUpdate,timeleft) 
                    
                for f in todo:
                    data = f.fetch()


        except:
            print 'Error in datafetcher threadfunction'
            if debug:
                raise

def init():
    settings = {}

    return (PLUGIN_NAME, settings, signalHandler, threadFunc)

