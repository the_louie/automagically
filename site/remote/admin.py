from remote.models import Theme, Remote, Widget, Page, SingleDevCmd, OnOffDev, DimDev, VariableValue, Link, Heading, GenericContent

from django.contrib import admin
from django.utils.safestring import mark_safe
from django.core.urlresolvers import reverse

class PageInline(admin.TabularInline):
    model = Page
    fk_name = 'remote'
    extra = 0
#    fieldsets = ((None, {'fields': ('name', 'number')}),)

    def admin_link(self, instance):
        url = reverse('admin:%s_%s_change' % (
            instance._meta.app_label,  instance._meta.module_name),  args=[instance.pk] )
        return mark_safe(u'<a href="{u}">Edit</a>'.format(u=url))

    fields = ('name', 'admin_link', 'number')
    readonly_fields = ('admin_link',)



class RemoteAdmin(admin.ModelAdmin):
    inlines = [
        PageInline,
        ]

class WidgetInline(admin.TabularInline):
    model = Widget
    fk_name = 'page'
    extra = 0
#    readonly_fields = ('displayText',)
    fields = ('displayText', 'y', 'x')
    ordering = ('y', 'x')

    
class PageAdmin(admin.ModelAdmin):
    list_display = ('remote', '__unicode__')
    list_display_links = ('__unicode__',)
    inlines = [
        WidgetInline,
        ]

class WidgetAdmin(admin.ModelAdmin):
    ordering = ('page', 'y', 'x')
    list_display = ('remote', 'page', '__unicode__', 'y', 'x')
    list_display_links = ('__unicode__',)
    list_filter = ('page__remote',)

#admin.site.register(Theme)
admin.site.register(Remote, RemoteAdmin)
#admin.site.register(Widget, WidgetAdmin)
admin.site.register(Page, PageAdmin)
admin.site.register(SingleDevCmd, WidgetAdmin)
admin.site.register(DimDev, WidgetAdmin)
admin.site.register(OnOffDev, WidgetAdmin)
admin.site.register(VariableValue, WidgetAdmin)
admin.site.register(Link, WidgetAdmin)
admin.site.register(Heading, WidgetAdmin)
admin.site.register(GenericContent, WidgetAdmin)


